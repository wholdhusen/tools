-- Number of vehicles with trips (aka active, not just test vins)
select count(distinct(vehicle_id)) from trip_log;

-- Number of users
select count(*) from user_account

-- Number of users active this week
select count(*) from user_account as u
where u.last_seen between clock_timestamp() + (-7 * interval '1 day')
and clock_timestamp();

-- Number of car models (with trips again to avoid test vins)
select count(distinct(v.model)) from vehicle as v
inner join trip_log as t on v.vehicle_id = t.vehicle_id

