select sum(t.meters_traveled) from trip_log as t
inner join event_metadata as e on e.metadata_id = t.end_metadata_id
where e.event_timestamp between clock_timestamp() + (-7 * interval '1 day')
and clock_timestamp();
