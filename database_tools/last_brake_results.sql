select distinct d1.final_run_timestamp, d1.vehicle_id, u.full_name
from deceleration_results as d1
inner join (select vehicle_id, max(final_run_timestamp) as last_time
    from deceleration_results group by vehicle_id) as d2
on d1.vehicle_id = d2.vehicle_id and d1.final_run_timestamp = d2.last_time
inner join user_vehicle_xref as x on x.vehicle_id = d1.vehicle_id
inner join user_account as u on u.user_id = x.user_id
order by d1.final_run_timestamp desc
