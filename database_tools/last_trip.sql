select distinct t1.event_timestamp, t1.vehicle_id, u.full_name
from (trip_log as t inner join event_metadata as e
    on t.end_metadata_id = e.metadata_id) as t1
inner join (select t.vehicle_id, max(e.event_timestamp) as last_time
    from trip_log as t inner join event_metadata as e
    on t.end_metadata_id = e.metadata_id
    group by t.vehicle_id) as t2
on t1.vehicle_id = t2.vehicle_id and t1.event_timestamp = t2.last_time
inner join user_vehicle_xref as x on x.vehicle_id = t1.vehicle_id
inner join user_account as u on u.user_id = x.user_id
order by t1.event_timestamp desc
