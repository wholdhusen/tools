select distinct r1.final_run_timestamp, r1.vehicle_id, u.full_name
from run_analysis_results as r1
inner join (select vehicle_id, max(final_run_timestamp) as last_time
    from run_analysis_results group by vehicle_id) as r2
on r1.vehicle_id = r2.vehicle_id and r1.final_run_timestamp = r2.last_time
inner join user_vehicle_xref as x on x.vehicle_id = r1.vehicle_id
inner join user_account as u on u.user_id = x.user_id
order by r1.final_run_timestamp desc
