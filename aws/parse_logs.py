import json
import boto3
import os
INPUT_FILE = 'descriptions.json'
OUTPUT_FILE = 'events.json'
GROUP_NAME = '/aws/lambda/v1-lambdas-car-fit-r-VehicleProviderGetvehicledata-9X20KIPV4Q7'
START_TIME = 1476489600000 # October 15th I think
""" HOWTO:
1) Install the AWS command line interface (pip install awscli)
2) Change all the all caps stuff to relevant stuff (group name is part of the Cloud Watch
   URL).
3) Remove old files if needed
3) Run this code.
4) Search within the OUTPUT_FILE (grep or what-have-you) for what you want to find.
"""
os.system('aws logs describe-log-streams --log-group-name {} > {}'.format(
        GROUP_NAME, INPUT_FILE))
f = open(INPUT_FILE,'r')
out = open(OUTPUT_FILE,'w')
logs = json.load(f)
logStreamNames = []
for ls in logs['logStreams']:
    logStreamNames = logStreamNames + [ls['logStreamName']]

print 'there are {} log stream names in the list'.format(len(logStreamNames))

client = boto3.client('logs')
responses = {'log_events':[]}
for lsn in logStreamNames:
    # get_log_events can filter by timestamp (optional arguments)
    response = client.get_log_events(
        logGroupName=GROUP_NAME,
        logStreamName=lsn, startTime=START_TIME)
    print 'response obtained for stream {}'.format(lsn)
    responses['log_events'] = responses['log_events'] + [response]
json.dump(responses, out, sort_keys=True, indent=4)
